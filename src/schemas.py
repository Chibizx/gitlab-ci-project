from typing import List

from pydantic import BaseModel, ConfigDict


class BaseIngredient(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    id: int
    name: str


class BaseRecipe(BaseModel):
    name: str
    cook_time: int


class IngredientSchema(BaseIngredient):
    recipes: List[BaseRecipe]


class RecipeSchema(BaseRecipe):
    model_config = ConfigDict(from_attributes=True)

    view_count: int


class RecipeDetail(BaseRecipe):
    model_config = ConfigDict(from_attributes=True)

    ingredients: List[BaseIngredient]
    description: str
