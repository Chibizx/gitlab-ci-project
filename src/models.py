from typing import List, Final
from sqlalchemy import Column, Table, String, Integer, ForeignKey

from sqlalchemy.orm import relationship, Mapped, mapped_column

from .database import Base


association_table: Final[Table] = Table(
    "association_table",
    Base.metadata,
    Column("recipe", ForeignKey("Recipe.id"), primary_key=True),
    Column("ingredient", ForeignKey("Ingredient.id", ondelete="CASCADE"), primary_key=True),
)


class Recipe(Base):
    __tablename__ = 'Recipe'

    id: Mapped[int] = mapped_column(Integer, primary_key=True, index=True)
    name: Mapped[str] = mapped_column(String, index=True)
    view_count: Mapped[int] = mapped_column(Integer, default=0, index=True)
    cook_time: Mapped[int] = mapped_column(Integer, nullable=False, index=True)
    ingredients: Mapped[List["Ingredient"]] = relationship("Ingredient", secondary=association_table, back_populates="recipes")
    description: Mapped[str] = mapped_column(String, index=True)

    def update_view_count(self) -> None:
        self.view_count += 1

    def __str__(self) -> str:
        return self.__repr__()

    def __repr__(self) -> str:
        return '<Recipe: id={0.id}, name={0.name}, description={0.description}>'.format(self)


class Ingredient(Base):
    __tablename__ = 'Ingredient'

    id: Mapped[int] = mapped_column(Integer, primary_key=True, index=True)
    name: Mapped[str] = mapped_column(String, unique=True, index=True)
    recipes: Mapped[List["Recipe"]] = relationship("Recipe", secondary=association_table, back_populates="ingredients")

    def __str__(self) -> str:
        return self.__repr__()

    def __repr__(self) -> str:
        return '<Ingredient: id={0.id}, name={0.name}>'.format(self)
