<<<<<<< HEAD
from typing import List, Sequence
from contextlib import asynccontextmanager
=======
from typing import List
>>>>>>> parent of 77dedd5... Problem with

from fastapi import FastAPI, Path, Depends
from sqlalchemy.future import select
from sqlalchemy.orm import joinedload
from sqlalchemy.ext.asyncio import AsyncSession

from .database import engine, async_session_global
from .models import Recipe, Base
from .schemas import RecipeSchema, RecipeDetail


<<<<<<< HEAD
@asynccontextmanager
async def lifespan(app: FastAPI):
=======
app = FastAPI()


async def get_async_session() -> AsyncSession:
    async with async_session_global() as session:
        yield session


async def get_one_or_create(session: Session,
                            model,
                            create_method='',
                            create_method_kwargs=None,
                            **kwargs):
    # Функция возвращает из таблицы элемент при наличии соответствуюего переданного атрибута
    # Если в таблице нет такого элемента, создает его
    try:
        res = await session.execute(select(model).filter_by(**kwargs))
        return res.scalars().one()
    except NoResultFound:
        kwargs.update(create_method_kwargs or {})
        created = getattr(model, create_method, model)(**kwargs)
        try:
            session.add(created)
            session.flush()
            return created
        except IntegrityError:
            session.rollback()
            res = await session.execute(select(model).filter_by(*kwargs))
            return res.scalars().one()


# TODO: Функцию добавил для проверки наличия ингредиентов в таблице,
#  для использования одного ингредиента в разных рецептах.
async def create_recipe_objects(
        recipe,
        session: Session,
) -> List[Recipe]:
    ingredients_in_recipe = [
        await get_one_or_create(session, Ingredient, name=ingredient_name) for ingredient_name in recipe["ingredients"]
    ]

    # for ingredient in recipe["ingredients"]:
        # string_in_table = await session.execute(select(Ingredient).where(Ingredient.name == ingredient))
        # result_obj = string_in_table.scalars().one_or_none()
        # if not result_obj:
        #     result_obj = Ingredient(name=ingredient)
        # ingredients_in_recipe.append(result_obj)

    result_recipes = Recipe(
        name=recipe["name"],
        cook_time=recipe["cook_time"],
        ingredients=ingredients_in_recipe,
        description=recipe["description"]
    )

    local_object = await session.merge(result_recipes)
    session.add(local_object)
    await session.commit()
    await session.refresh(result_recipes)


@app.on_event("startup")
async def startup():
>>>>>>> parent of 77dedd5... Problem with
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

    yield

    await engine.dispose()

app = FastAPI(lifespan=lifespan)


async def get_async_session() -> AsyncSession:
    async with async_session_global() as session:
        yield session


@app.get("/cookbook/{recipe_id}/", response_model=RecipeDetail)
async def recipe_detail(
        recipe_id: int = Path(
            ...,
            title='The id of the recipe to get detailed information about.',
        ),
        session: AsyncSession = Depends(get_async_session),
) -> Recipe:
    res = await session.execute(select(Recipe)
                                .options(joinedload(Recipe.ingredients))
                                .where(Recipe.id == recipe_id))
    recipe_item = res.unique().scalars().one()
    recipe_item.update_view_count()
    await session.commit()
    return recipe_item


@app.get("/cookbook", response_model=List[RecipeSchema])
async def cookbook(
<<<<<<< HEAD
        session: AsyncSession = Depends(get_async_session),
) -> Sequence[Recipe]:
=======
        session: Session = Depends(get_async_session),
) -> List[Recipe]:
>>>>>>> parent of 77dedd5... Problem with
    res = await session.execute(select(Recipe).order_by(-Recipe.view_count))
    return res.scalars().all()
