import pytest
from sqlalchemy import func

from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from src.models import Recipe, Ingredient


pytestmark = pytest.mark.asyncio


async def test_main_cookbook(
        async_db_engine,
        async_client: AsyncClient,
        async_db: AsyncSession,
        async_example_orm,
) -> None:
    response = await async_client.get("/cookbook")

    assert response.status_code == 200


async def test_first_recipe(
        async_db_engine,
        async_client: AsyncClient,
        async_db: AsyncSession,
        async_example_orm,
) -> None:
    response = await async_client.get("/cookbook/1/")
    response_data = response.json()

    expected_result = {
        "name": "omlet",
        "cook_time": 15,
        "description": "simple omlet"
    }
    milk = {"id": 1, "name": "milk"}
    egg = {"id": 2, "name": "egg"}

    response_ingredients = response_data.pop("ingredients")

    assert milk in response_ingredients and egg in response_ingredients
    assert expected_result == response_data
    assert response.status_code == 200


async def test_second_recipe(
        async_db_engine,
        async_client: AsyncClient,
        async_db: AsyncSession,
        async_example_orm,
) -> None:
    response = await async_client.get("/cookbook/2/")
    response_data = response.json()

    expected_result = {
        "name": "cake",
        "cook_time": 50,
        "description": "apple cake"
    }
    milk = {"id": 2, "name": "egg"}
    egg = {"id": 3, "name": "apple"}

    response_ingredients = response_data.pop("ingredients")

    assert milk in response_ingredients and egg in response_ingredients
    assert expected_result == response_data
    assert response.status_code == 200


async def test_database_rows(
        async_db_engine,
        async_client: AsyncClient,
        async_db: AsyncSession,
        async_example_orm,
) -> None:
    amount_ingredients = await async_db.execute(func.count(Ingredient.id))

    assert 3 == amount_ingredients.scalar()

    amount_recipes = await async_db.execute(func.count(Recipe.id))

    assert 2 == amount_recipes.scalar()
