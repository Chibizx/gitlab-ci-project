from typing import List

import pytest_asyncio

from sqlalchemy import text
from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, AsyncEngine
from sqlalchemy.engine.result import ScalarResult
from sqlalchemy.future import select
from httpx import AsyncClient

from conf_test_db import engine

from src.database import Base
from src.models import Recipe, Ingredient
from src.main import app, get_async_session


@pytest_asyncio.fixture(scope='session')
async def async_db_engine() -> AsyncEngine:
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

    yield engine

    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)

    await engine.dispose()


@pytest_asyncio.fixture(scope='function')
async def async_db(async_db_engine, async_client) -> AsyncSession:
    async_session_for_test = async_sessionmaker(
        bind=async_db_engine,
        expire_on_commit=False,
        autocommit=False,
        autoflush=False,
    )

    async def override_get_db() -> AsyncSession:
        async with async_session_for_test() as test_session:
            yield test_session

    app.dependency_overrides[get_async_session] = override_get_db

    async with async_session_for_test() as session:
        await session.begin()

        yield session

        await session.rollback()

        for table in reversed(Base.metadata.sorted_tables):
            table_name: str = table.name
            await session.execute(text(f'DELETE FROM {table_name};'))
            await session.commit()


@pytest_asyncio.fixture(scope='session')
async def async_client() -> AsyncClient:
    app.lifespan = None

    async with AsyncClient(app=app, base_url='http://localhost:8000') as client:
        yield client


@pytest_asyncio.fixture
async def async_example_orm(async_db: AsyncSession) -> List[Recipe]:
    async def get_one_or_create(session: AsyncSession,
                                model,
                                create_method='',
                                create_method_kwargs=None,
                                **kwargs) -> ScalarResult:
        try:
            res = await session.execute(select(model).filter_by(**kwargs))
            return res.scalars().one()
        except NoResultFound:
            kwargs.update(create_method_kwargs or {})
            created = getattr(model, create_method, model)(**kwargs)
            try:
                session.add(created)
                await session.flush()
                return created
            except IntegrityError:
                await session.rollback()
                res = await session.execute(select(model).filter_by(*kwargs))
                return res.scalars().one()

    test_recipes_dict = [
        {
            "name": "omlet",
            "cook_time": 15,
            "ingredients": (
                "milk",
                "egg",
            ),
            "description": "simple omlet",
        },
        {
            "name": "cake",
            "cook_time": 50,
            "ingredients": (
                "apple",
                "egg",
            ),
            "description": "apple cake",
        },
    ]
    result_recipes = []

    for recipe in test_recipes_dict:
        ingredients_for_recipe = [
            await get_one_or_create(
                async_db, Ingredient,
                name=ingredient_name,
            ) for ingredient_name in recipe["ingredients"]
        ]
        result_recipes.append(
            Recipe(
                name=recipe["name"],
                cook_time=recipe["cook_time"],
                ingredients=ingredients_for_recipe,
                description=recipe["description"]
            )
        )

        async_db.add_all(result_recipes)
        await async_db.commit()

    return result_recipes
