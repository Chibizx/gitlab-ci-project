from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker

from src.database import Base
from src.main import app, get_async_session


SQLALCHEMY_DATABASE_URL = "sqlite+aiosqlite:///./app_test.db"

engine = create_async_engine(SQLALCHEMY_DATABASE_URL)
